function feed() {
	console.log("Request handler 'feed' was called.");
}

function upload() {
	console.log("Request handler 'upload' was called.");
}

exports.feed = feed;
exports.upload = upload;