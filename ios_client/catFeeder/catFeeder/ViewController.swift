//
//  ViewController.swift
//  catFeeder
//
//  Created by Anne Saints on 25/11/2017.
//  Copyright © 2017 Anne Saints. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func feedBtn(_ sender: Any) {
        let String = "http://172.18.37.124:8888/feed"
    let url = URL(string: String)!
        
        //now create the URLRequest object using the url object
    let request = URLRequest(url: url)
    
    //create the session object
    let session = URLSession.shared
    
    //create dataTask using the session object to send data to the server
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        
        guard error == nil else {
            return
        }
        
        print("Success")
    })
    
    task.resume()
    
    }
    
}

