function feed() {
	console.log("Request handler 'feed' was called.");
	// invoke the feed script
	var PythonShell = require('python-shell');

	console.log("Running CatFeeder.py script.");
	PythonShell.run('CatFeeder.py', function (err) {
	  if (err) throw err;
	  console.log('CatFeeder.py finished');
	});
}

function upload() {
	console.log("Request handler 'upload' was called.");
}



exports.feed = feed;
exports.upload = upload;