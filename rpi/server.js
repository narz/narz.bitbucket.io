var http = require("http");
var url = require("url");

function start(route, handle) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Request for " + pathname + " received.");

		route(handle, pathname);

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write("Hello World");
		response.end();
	}

	http.createServer(onRequest).listen(8888);
	console.log("Server has started.");
}

exports.start = start;

// createServer() This function returns an object, and this object has a method named listen, and takes a numeric value which indicates the port number our HTTP server is going to listen on. 

// request object

// url module - provides methods which allow us to extract the different parts of a URL
// queryString - can in turn be used to parse the query string for request parameters:

// response object

// response.writeHead()

