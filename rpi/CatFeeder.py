
#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

def feed():
    # let the GPIO library know where we've connected our servo to the Pi
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(18, GPIO.OUT)
    
    try:
        #18 is the GPIO port that creates the PWM signal; 50hertz is the required freq of the PWM signal)
        servo = GPIO.PWM(18, 50)
        #12.5% is the initial duty cycle
        servo.start(12.5)
        
        # spin left or open, right or close
        # to prevent the food from jamming the servo
        # 0 is the starting position, there are 3 indeces, namely 0,1,2
        for index in range(0, 3):
            # dutycycle is position 2.5=0 deg 7.5=90 deg 12.5=180 deg
            dutyCycle = 2.5 if (index % 2 == 0) else 12.5
            servo.ChangeDutyCycle(dutyCycle)
            # adjust the sleep time to have the servo spin longer or shorter in that direction
            time.sleep(3)
    finally:
        # always cleanup after ourselves
        servo.stop()
        GPIO.cleanup()

if __name__ == '__main__':
    # kick off the feeding process (move the servo)
    feed()

